console.log("Hello World!");

//-----//
let num = 2;
let getCube = num ** 3;
console.log(`The cube of ${num} is ${getCube}.`);

//-----//
const address = ["258 Washington Ave", "NW","California","90011"];
const [street,city,state,zipCode] = address;
console.log(`I live at ${street} ${city}, ${state} ${zipCode}`);

//-----//
const animal = {
    name: "Lolong",
    type: "saltwater crocodile",
    weight: "1075 kgs",
    measurement: "20 ft 3 in"
};

const {name,type,weight,measurement} = animal;
console.log(`${name} was a ${type}. He weighed at ${weight} with a measurment of ${measurement}.`);

//-----//
const numbers = [1,2,3,4,5];
numbers.forEach((number) => console.log(number));

let reduceNumber = numbers.reduce((previousValue, currentValue) => previousValue + currentValue);
console.log(reduceNumber);

//-----//
class Dog{
    constructor(name,age,breed){
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
};
const myDog = new Dog("Frankie","5","Miniature Dachshund");
console.log(myDog);


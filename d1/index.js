console.log("Hello World!");

// SECTION - Exponent operator
// pre-ES6
/**
 * syntax:
 * let/const variableName = Math.pow(number,exponent);
 */
let firstNum = Math.pow(8,2);
console.log(firstNum);

// ES6
/**
 * syntax:
 * let/const variableName = number ** exponent
 */
let secondNum = 8 ** 2;
console.log(secondNum);

// SECTION - Template Literals
/**
 * allows to write strings without using the concatination operator (+);
 */
let name = "John";

// pre-ES6
let message = "Hello "+ name +"! Welcome to programming!";
console.log("Message without template literals: "+message);
//ES6
message = `Hello ${name}! Welcome to programming`;
console.log(`Message without template literals: ${message}`);
// multiple-line
const anotherMessage = `
${name} won the math competition. He won it by solving the problem 8 ** 2 with the solution of ${8 ** 2}.

`;

console.log(anotherMessage);
// MINI ACTIVITY
/**
 * create 2 variables interesRate which has the value of 0.15;
 * principal which has the value of 1000
 * 
 * log in the console the total interest of the account that has the principal as its balance and the interestRate as its interest %.
 * -template literals allow us to write strings with embedded JS expressions
 * -expressions in general are any valid unit of code that resolves into a value
 * syntax : `${}`
 * 
 */
let interestRate = 0.15;
let principal = 1000;
console.log(`The total interest is ${principal*interestRate}.`);

// SECTION - ARRAY DESTRUCTURING

const fullName = ['Juan',"Dela","Cruz"];
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

// array destructuring
/***
 * allows us to unpack elements in arrays to distinct variables
 * allows us to name array elements with variables instead of using index numbers
 * syntax: let/const [variableA,varibaleB,variableC]=arrayName
 */
const [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);
//using template literals
console.log(`Hello ${firstName} ${middleName} ${lastName}!`);

// pre-ES6/pre-object destructuring
let person ={
    givenName: "Jane",
    maidenName: "Dela",
    familyName: "Cruz"
};
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);
console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}!`);

// ES6 object destructuring
let {givenName,maidenName,familyName} = person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);
console.log(`Hello ${givenName} ${maidenName} ${familyName}!`);

function getFullName({givenName,maidenName,familyName}){
    console.log(`Hello ${givenName} ${maidenName} ${familyName}!`);
}
getFullName(person);

const pet = {
    name: "Guard",
    trick: "Play dead",
    treat: "carrot"
}
function getPet({name,trick,treat}){
    console.log(`${name}, ${trick}!`);
    console.log(`Good ${trick}!`);
    console.log(`Here is ${treat} for you!`);
}
getPet(pet);

// SECTION ARROW FUNCTION

// const hello = () => {
//     console.log("Hello World");
// };
// pre-ES6 arrow function
// const printFullName = (firstName,middleName,lastName) => {
//     console.log(`${firstName} ${middleName} ${lastName}`);
// }
// printFullName("Portgas", "D.", "Ace");

// ES6 arrow function
const printFullName = (firstName,middleInitial,lastName) => {
    console.log(`${firstName} ${middleInitial} ${lastName}`);
}

printFullName("Portgas", "D.", "Ace");

// pre-arrow function
const students = ["John","Jane","Judy"];
students.forEach(function(student){
    console.log(`${student} is a student.`);

});

// SECTION - IMPLICIT RETURN STATEMENT

// let add = (x,y) => {
//     return x + y;
// }
// console.log(add(12,1));

// ES6
const add = (x,y) => x+y;
let addVariable = add(99,888);
console.log(addVariable);

// SECTION - Default Argument Value
const greet = (name = "User") =>{
    return `Good morning ${name}!`
};
console.log(greet("John"));
console.log(greet("Alex"));

// SECTION - CLASS-BASED OBJECT BLUE PRINTS
/**
 * Allows creation/instantiation of objects using classes as blueprints
 * 
 * creating a class
 *  - constructor is a special method of class for creating/initializing an object for that class
 *  - "this" keyword refers to the properties of an object created from the class, this allows us to reassing values for the properties inside the class
 * SYNTAX:
 *      class className{
 *          constructor(objectPropertyA,objectPropertyB){
 *              this.objectPropertyA = objectPropertyA;
 *              this.objectPropertyB = objectPropertyB;
 *           }
 *      }
 */
class Car{
    constructor(brand,name,year){
        this.brand = brand;
        this.name = name;
        this.year = year;
    }
};
// first instance using initializer and/or dot notation, create a carobject using the car calss that we have create.

const myCar = new Car();
myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;
console.log(myCar);

// second instance
const myNewCar = new Car("Nissan","Terra","2022");
console.log(myCar);